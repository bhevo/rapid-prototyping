using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Correct : MonoBehaviour
{
    private float moveSpeed = 3.0f;
    public GameObject nextLevel;
    public Transform nextPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void correctAnswer()
    {
        transform.Translate(Vector3.up * Time.deltaTime * moveSpeed);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            correctAnswer();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Instantiate(nextLevel, nextPos);
    }
}
