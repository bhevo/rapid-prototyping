using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incorrect : MonoBehaviour
{
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void wrong()
    {

        transform.Translate(Vector3.down * Time.deltaTime * moveSpeed);
    }

   // private void OnTriggerEnter(Collider other)
   // {
    //    if (other.CompareTag("Player"))
    //    {
     //       wrong();
    //    }
  //  }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            wrong();
        }
    }
}
