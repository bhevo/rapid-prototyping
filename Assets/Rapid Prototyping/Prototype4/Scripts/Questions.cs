using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Questions : MonoBehaviour
{
    public Text questionsTxt;
    public Text a1;
    public Text b1;
    public Text c1;
    public GameObject levelPrefab;
    public Transform nextPos;

    public GameObject cube1;
    public GameObject cube2;
    public GameObject cube3;

   // private float moveSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("question1"))
        {
            questionsTxt.text = "Question: 1 + 1 = ? ";
            a1.text = "0";
            b1.text = "2";
            c1.text = "21";
        }

        if (other.CompareTag("question2"))
        {
            questionsTxt.text = "Solve for x : 10(x-(2x-41)-5x)/(11x-7) = 5 ";
            a1.text = "x = 89/23";
            b1.text = "x = -23/3";
            c1.text = "x = 76/61";
            

        }
       // Instantiate(levelPrefab, nextPos);
    }
}
