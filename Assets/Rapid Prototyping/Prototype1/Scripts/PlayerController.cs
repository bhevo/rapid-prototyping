using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Prototype1
{
    public class PlayerController : MonoBehaviour
    {
        private float speed = 15.0f;
        private Rigidbody playerRb;
        private GameObject focalPoint;
        public bool hasPowerup = false;
        public bool hasDebuff = false;
        private float powerUpStrength = 15.0f;
        private float debuffStrength = -5.0f;
        public GameObject powerupIndicator;
        public GameObject debuffIndicator;

        // Start is called before the first frame update
        void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            focalPoint = GameObject.Find("Focal Point");
        }

        // Update is called once per frame
        void Update()
        {
            

            if (transform.position.y < -5)
            {
                SceneManager.LoadScene(0);
                
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
                Debug.Log("Quit");
            }

        }
        private void FixedUpdate()
        {
            float fowardInput = Input.GetAxis("Vertical");
            playerRb.AddForce(focalPoint.transform.forward * fowardInput * speed);
            powerupIndicator.transform.position = transform.position;
            debuffIndicator.transform.position = transform.position;
        }



        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Powerup"))
            {
                hasPowerup = true;
                powerupIndicator.gameObject.SetActive(true);
                Destroy(other.gameObject);
                StartCoroutine(PowerupContdownRoutine());
            }

            if (other.CompareTag("Debuff"))
            {
                hasDebuff = true;
                debuffIndicator.gameObject.SetActive(true);
                Destroy(other.gameObject);
                StartCoroutine(DebuffCountdownRoutine());
            }
        }

        IEnumerator PowerupContdownRoutine()
        {
            yield return new WaitForSeconds(4);
            hasPowerup = false;
            powerupIndicator.gameObject.SetActive(false);
        }

        IEnumerator DebuffCountdownRoutine()
        {
            yield return new WaitForSeconds(4);
            hasDebuff = false;
            debuffIndicator.gameObject.SetActive(false);
        }


        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Enemy")&& hasPowerup)
            {
                Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
                enemyRigidbody.AddForce(awayFromPlayer * powerUpStrength, ForceMode.Impulse);
                Debug.Log("Collided with: " + collision.gameObject.name + " with powerup set to " + hasPowerup);
            }

            if (collision.gameObject.CompareTag("Enemy") && hasDebuff)
            {
                Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
                enemyRigidbody.AddForce(awayFromPlayer * debuffStrength, ForceMode.Impulse);
                Debug.Log("Collided with: " + collision.gameObject.name + " with powerup set to " + hasDebuff);
            }
        }
    }

}
