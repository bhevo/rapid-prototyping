using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Prototype3
{
    public class PlayerMovement : MonoBehaviour
    {
        public GameObject topLeftTarget;
        public GameObject bottomLeftTarget;
        public GameObject topRightTarget;
        public GameObject bottomRightTarget;
        public int moveAmount;
        public Text movesMadeText;


        void Start()
        {
            moveAmount = 0;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Goal"))
            {
                SceneManager.LoadScene(0);
            }

            if (other.CompareTag("Enemy"))
            {
                Debug.Log("You Died!");

                SceneManager.LoadScene(2);
            }
        }



        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                moveAmount = moveAmount + 1;
            }

            if (Input.GetMouseButton(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 700))
                {
                    //Debug.Log(hit.transform.name);
                    //Debug.Log("hit");
                    if (hit.collider.tag == "topLeft")
                    {
                        //Debug.Log("Top Left Was Hit");
                        transform.RotateAround(topLeftTarget.transform.position, Vector3.forward, 90 * Time.deltaTime);

                    }
                    else if (hit.collider.tag == "topRight")
                    {
                        //Debug.Log("Top Right Was Hit");
                        transform.RotateAround(topRightTarget.transform.position, Vector3.forward, 90 * Time.deltaTime);

                    }
                    else if (hit.collider.tag == "bottomLeft")
                    {
                        //Debug.Log("Bottom Left Was Hit");
                        transform.RotateAround(bottomLeftTarget.transform.position, Vector3.forward, 90 * Time.deltaTime);

                    }
                    else if (hit.collider.tag == "bottomRight")
                    {
                        //Debug.Log("Bottom Right Was Hit");
                        transform.RotateAround(bottomRightTarget.transform.position, Vector3.forward, 90 * Time.deltaTime);
                    }
                    movesMadeText.text = "Moves Made: " + moveAmount;
                }
            }

            if (Input.GetMouseButton(1))
            {
                Ray ray2 = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit2;
                if (Physics.Raycast(ray2, out hit2, 700))
                {
                    //Debug.Log(hit.transform.name);
                    //Debug.Log("hit");
                    if (hit2.collider.tag == "topLeft")
                    {
                        //Debug.Log("Top Left Was Hit");
                        transform.RotateAround(topLeftTarget.transform.position, Vector3.forward, -90 * Time.deltaTime);

                    }
                    else if (hit2.collider.tag == "topRight")
                    {
                        //Debug.Log("Top Right Was Hit");
                        transform.RotateAround(topRightTarget.transform.position, Vector3.forward, -90 * Time.deltaTime);

                    }
                    else if (hit2.collider.tag == "bottomLeft")
                    {
                        //Debug.Log("Bottom Left Was Hit");
                        transform.RotateAround(bottomLeftTarget.transform.position, Vector3.forward, -90 * Time.deltaTime);

                    }
                    else if (hit2.collider.tag == "bottomRight")
                    {
                        //Debug.Log("Bottom Right Was Hit");
                        transform.RotateAround(bottomRightTarget.transform.position, Vector3.forward, -90 * Time.deltaTime);

                    }
                }
            }
        }
    }
}      
