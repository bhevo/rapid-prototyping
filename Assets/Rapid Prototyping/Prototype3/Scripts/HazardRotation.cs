using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardRotation : MonoBehaviour
{
    // Start is called before the first frame update
    float speed = 100f;
    float delta = 1f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float y = transform.position.y + Mathf.PingPong(speed * Time.deltaTime, delta);
        Vector3 pos = new Vector3(transform.position.x, y, transform.position.z);
        transform.position = pos;
    }

    private void FixedUpdate()
    {
        transform.Rotate(0, 10, 0 * Time.deltaTime);
    }
}
