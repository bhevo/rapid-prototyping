using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace Prototype3
{
    public class Win : MonoBehaviour
    {
        public PlayerMovement playerMovement;
        
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Goal"))
            {
                Debug.Log("You won with " + playerMovement.moveAmount + " Moves!");
            }
        }
    }
}
