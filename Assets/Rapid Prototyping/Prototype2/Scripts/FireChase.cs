using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireChase : MonoBehaviour

{
    public GameObject player;
    public Rigidbody fireRb;
    public float speed;

    private void Start()
    {
        
    }

    private void FixedUpdate()
    {
        
    }

    private void Update()
    {
        Vector3 lookDirection = (player.transform.position - transform.position);
        fireRb.AddForce(lookDirection * speed * Time.deltaTime);
    }
}

