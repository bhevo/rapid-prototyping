using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager1 : MonoBehaviour
{
    // Start is called before the first frame update
    public Text scoreText;
    public float scoreCount;
    public float pointsPerSecond;
    public bool scoreIncreasing;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreCount += pointsPerSecond * Time.deltaTime;
        scoreText.text = "Score: " + scoreCount;
    }
}
