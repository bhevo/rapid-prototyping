using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    public float moveSpeed;
    public float jumpHeight;
    Rigidbody rb;
    Vector3 jumpH = new Vector3(0, 1, 0);
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /* private void FixedUpdate()
     {
         if (Input.GetKey(KeyCode.W))
         {
             rb.velocity = new Vector2(0, jumpHeight);
         }
         if (Input.GetKey(KeyCode.S))
         {
             rb.velocity = new Vector2(0, -jumpHeight);
         }


         if (Input.GetKey(KeyCode.A))
         {
             rb.velocity = new Vector2(-moveSpeed, 0);
         }

         if(Input.GetKey(KeyCode.D))
         {
             rb.velocity = new Vector2(moveSpeed, 0);
         }
     }*/
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.W))
        {
            // rb.AddForce(Vector3.up, ForceMode.Impulse);
            rb.AddForce(transform.up * jumpHeight, ForceMode.Impulse ) ;
        }

        if (Input.GetKey(KeyCode.D))
        {
            // rb.AddForce(Vector3.up, ForceMode.Impulse);
            rb.AddForce(transform.right * moveSpeed, ForceMode.VelocityChange);
        }

        if (Input.GetKey(KeyCode.S))
        {
            // rb.AddForce(Vector3.up, ForceMode.Impulse);
            rb.AddForce(-transform.up * jumpHeight, ForceMode.Impulse);
        }
    }
}
