using UnityEngine;
using UnityEngine.SceneManagement;
namespace Prototype2
{
    public class PlayerController : MonoBehaviour
    {

        //public bool hasPowerup = false;
        public float powerupStrength = 0.05f;
        public Playermovement playerMovement;
        public FireChase fireChase;
        public ScoreManager scoreManager;
        private Vector3 playerScale = new Vector3(1,1,1);
        private void OnCollisionEnter(Collision collision)
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
                Debug.Log("Quit");
            }

            if (collision.gameObject.CompareTag("Ground"))
            {

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Powerup"))
            {
                //moveSpeed = moveSpeed * powerupStrength;
                playerMovement.moveSpeed = playerMovement.moveSpeed + powerupStrength/2;
                transform.localScale = transform.localScale + playerScale;
                fireChase.speed = fireChase.speed + powerupStrength;
                scoreManager.score = scoreManager.score + 1.0f ;
                Debug.Log(scoreManager.score);
                
                Destroy(other.gameObject);
            }
        }







    }
}
